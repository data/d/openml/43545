# OpenML dataset: Adoptable-Dogs

https://www.openml.org/d/43545

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was created when I practiced webscraping.
Content
The data is a compilation of information on dogs who were available for adoption on December 12, 2019 in the Hungarian Database of Homeless Pets. In total, there were 2,937 dogs in the database. It contains information on dogs' names, breed, color, age, sex, the date they were found, and some characteristics of their personalities.
Inspiration
I thought it would be interesting to have a dataset that looks at adoptable dogs' characteristics. It is not really well-suited for prediction, but could be a good practice dataset for data visualization and working with categorical data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43545) of an [OpenML dataset](https://www.openml.org/d/43545). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43545/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43545/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43545/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

